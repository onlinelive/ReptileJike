/**
 * @author:稀饭
 * @time:下午9:31:44
 * @filename:video.java
 */
package bean;

public class Video {
	private String name;
	private String url;
	private String path;

	public Video(String name, String url, String path) {
		super();
		this.name = name;
		this.url = url;
		this.path = path;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

}
